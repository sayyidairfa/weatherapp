import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../data/image_path.dart';
import '../services/location_provider.dart';
import '../services/weather_service_provider.dart';
import '../utils/apptext.dart';
import '../utils/weather_card.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    final locationProvider =
        Provider.of<LocationProvider>(context, listen: false);
    locationProvider.determinePosition().then((_) {
      if (locationProvider.currentLocationName != null) {
        var city = locationProvider.currentLocationName!.locality;
        if (city != null) {
          Provider.of<WeatherServiceProvider>(context, listen: false)
              .fetchWeatherDataByCity(city.toString());
        }
      }
    });

    super.initState();
  }

  TextEditingController _cityController = TextEditingController();
  bool _clicked = false;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    final locationProvider = Provider.of<LocationProvider>(context);

    // Get the weather data from the WeatherServiceProvider
    final weatherProvider = Provider.of<WeatherServiceProvider>(context);
// Inside the build method of your _HomePageState class

// Get the sunrise timestamp from the API response
    int sunriseTimestamp = weatherProvider.weather?.sys?.sunrise ??
        0; // Replace 0 with a default timestamp if needed
    int sunsetTimestamp = weatherProvider.weather?.sys?.sunset ??
        0; // Replace 0 with a default timestamp if needed

// Convert the timestamp to a DateTime object
    DateTime sunriseDateTime =
        DateTime.fromMillisecondsSinceEpoch(sunriseTimestamp * 1000);
    DateTime sunsetDateTime =
        DateTime.fromMillisecondsSinceEpoch(sunsetTimestamp * 1000);

// Format the sunrise time as a string
    String formattedSunrise = DateFormat.Hm().format(sunriseDateTime);
    String formattedSunset = DateFormat.Hm().format(sunsetDateTime);
    return Scaffold(
      backgroundColor: Colors.black,
      extendBodyBehindAppBar: true,
      appBar: AppBar(),
      body: Container(
        padding: EdgeInsets.only(top: 45, left: 20, right: 20, bottom: 20),
        height: size.height,
        width: size.width,
        decoration: BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage(
                  background[weatherProvider.weather?.weather![0]?.main ??
                          "N/A"] ??
                      "assets/images/background_image.png",
                ))),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              WeatherCard(
                height: 320,
                width: 360,
                color: Color(0xFFAC736A),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          imagePath[
                                  weatherProvider.weather?.weather![0]?.main ??
                                      "N/A"] ??
                              "assets/img/default.png",
                          height: 85,
                          width: 95,
                          color: Color(0xFFF6C8A4),
                        ),
                        AppText(
                          data:
                              "${weatherProvider.weather?.main?.temp?.toStringAsFixed(0)} \u00B0" ??
                                  "", // Display temperature
                          color: Color(0xFFF6C8A4),
                          fw: FontWeight.bold,
                          size: 90,
                        ),
                      ],
                    ),
                    AppText(
                      data: weatherProvider.weather?.weather![0].main ?? "N/A",
                      color: Color(0xFFF6C8A4),
                      fw: FontWeight.w600,
                      size: 20,
                    ),
                    SizedBox(height: 5),
                    AppText(
                      data: weatherProvider.weather?.name ?? "N/A",
                      color: Color(0xFFF6C8A4),
                      fw: FontWeight.w500,
                      size: 15,
                    ),
                    SizedBox(height: 5),
                    AppText(
                      data: DateFormat('hh:mm a').format(DateTime.now()),
                      color: Color(0xFFF6C8A4),
                      fw: FontWeight.w500,
                      size: 15,
                    ),
                    SizedBox(height: 5),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        AppText(
                          data:
                              "Feels like ${weatherProvider.weather?.main!.feelsLike!.toStringAsFixed(0)} \u00B0C" ??
                                  "N/A",
                          color: Color(0xFFF6C8A4),
                          size: 15,
                          fw: FontWeight.w500,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                          child: Container(
                              height: 10, width: 1, color: Color(0xFFF6C8A4)),
                        ),
                        SizedBox(height: 5),
                        AppText(
                          data: "Sunset ${formattedSunset} " ?? "N/A",
                          color: Color(0xFFF6C8A4),
                          size: 15,
                          fw: FontWeight.w500,
                        ),
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(height: 20),
              Container(
                  height: 193,
                  width: 360,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      gradient: RadialGradient(
                        colors: [
                          Color(0x66AC736A),
                          Color(0xCCAC736A),
                        ],
                      )),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CustomColumn('Now', '25°C'),
                          SizedBox(width: 8),
                          CustomColumn('2 AM', '25°C'),
                          SizedBox(width: 8),
                          CustomColumn('3 AM', '23°C'),
                          SizedBox(width: 8),
                          CustomColumn('4 AM', '23°C'),
                          SizedBox(width: 8),
                          CustomColumn('5 AM', '20°C'),
                        ],
                      ),
                      SizedBox(height: 15),
                      Divider(
                        indent: 30,
                        endIndent: 30,
                      ),
                      SizedBox(height: 15),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CustomColumn('6 AM', '25°C'),
                          SizedBox(width: 8),
                          CustomColumn('7 AM', '25°C'),
                          SizedBox(width: 8),
                          CustomColumn('8 AM', '23°C'),
                          SizedBox(width: 8),
                          CustomColumn('9 AM', '22°C'),
                          SizedBox(width: 8),
                          CustomColumn('10 AM', '20°C'),
                        ],
                      )
                    ],
                  )),
              SizedBox(height: 20),
              AppText(data: 'Random Text', size: 20, color: Colors.white),
              AppText(
                  data: 'Improve him believe opinion offered met and'
                      'end cheered forbade. Friendly as stronger'
                      'speedily by recurred. Son interest wandered'
                      'sir addition end say. Manners beloved affixed'
                      'picture men ask.',
                  // data: weatherProvider.weather?.weather![0].description ?? "N/A",
                  size: 15,
                  color: Colors.white)
            ],
          ),
        ),
      ),
    );
  }

  Widget CustomColumn(String? text1, String? text2) {
    return Column(
      children: [
        AppText(data: text1, size: 15, color: Colors.white),
        Row(children: [
          Icon(Icons.cloud, size: 12),
          SizedBox(width: 5),
          AppText(data: text2, size: 15, color: Colors.white)
        ])
      ],
    );
  }
}
