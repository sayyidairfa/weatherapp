import 'package:flutter/material.dart';

class WeatherCard extends StatelessWidget {
  final double? height;
  final double? width;
  final Color? color;
  final double? borderRadius;
  final EdgeInsets? margin;
  final EdgeInsets? padding;
  final BoxDecoration? decoration;
  final Widget? child;



  WeatherCard({super.key,
    this.height,
    this.width,
    this.color,
    this.decoration,
    this.borderRadius,

    this.margin,
    this.padding,
    this.child,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(borderRadius ?? 35),

      ),
      margin: margin,
      padding: padding,
      child: child,
    );
  }
}
